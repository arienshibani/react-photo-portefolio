# Shibani Photo [![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](https://shibaniphoto.web.app/)

B/W 120mm Photography portefolio

[![React !](https://badges.aleen42.com/src/react.svg)](https://shibaniphoto.web.app/)
[![npm !](https://badges.aleen42.com/src/npm.svg)](https://www.npmjs.com/)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html) [![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](mailto:aaari94@gmail.com)

# Development

For local development on the website, clone the repository.

```bash
git clone https://arishibani@bitbucket.org/arienshibani/react-photo-portefolio.git
```

Open a terminal in the repository's folder and execute the following:

- Type in `yarn install` to install all the project dependancies.
- Type in `yarn start` to serve the website locally.

# Deployment

- Simply `git push` any changes to update production.
  - Builds to [Firebase](https://firebase.google.com/docs) on each commit.

```yaml
# /src/master/bitbucket-pipelines.yml
- pipe: atlassian/firebase-deploy:1.2.0
  variables:
    PROJECT_ID: shibaniphoto
    FIREBASE_TOKEN: $CLIToken
    MESSAGE: "Deploying react-photo-portefolio to prod."
```
