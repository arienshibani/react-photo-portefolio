import React from "react";
import styled from "styled-components";

const Thumbnail = ({ imgUrl, handleClick, index }) => {
  return (
    <ThumbnailImage
      src={imgUrl}
      onClick={handleClick}
      data-index={index}
      alt="colorless photograph"
    />
  );
};

const ThumbnailImage = styled.img`
  max-width: 12.5vw;
  max-height: 3.5vh;
  justify-content: center;
  margin-left: 3px;
  margin-right: 3px;
  filter: invert(100%);
  display: flex;


  ::selection {
  background: white; /* WebKit/Blink Browsers */
  }

  &:hover {
    filter: invert(0%);

  }
  &:active {
    filter: invert(0%);
  }

  @media only screen and (max-width: 420px) {
    max-width: 17.5vw;
    max-height: 19.5vh;
    margin: 2px;
  }
}
`;

export default Thumbnail;
