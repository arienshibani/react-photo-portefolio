import React from "react";
import Thumbnail from "./thumbnail";
import styled from "styled-components";

const ThumbnailGrid = ({ thumbnails, handleClick }) => {
  return (
    <ThumbnailContainer>
      {//For each thumbnail in state, render a thumbnail component.
      thumbnails.map((thumbnail, i) => {
        return (
          <Thumbnail
            key={thumbnail.imgUrl}
            imgUrl={thumbnail.imgUrl}
            handleClick={handleClick}
            index={i}
          />
        );
      })}
    </ThumbnailContainer>
  );
};

const ThumbnailContainer = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow-x: auto;
  border: 1px black;
  ::selection {
    background: white; /* WebKit/Blink Browsers */
  }

  &::-webkit-scrollbar {
    display: none;
  }

  @media only screen and (max-width: 420px) {
    margin-top: 5vh;
  }
`;

export default ThumbnailGrid;
