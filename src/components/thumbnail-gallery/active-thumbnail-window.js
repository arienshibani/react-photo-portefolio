import React from "react";
import styled from "styled-components";

const ActiveThumbnailWindow = ({ activeThumbnail }) => {
  return (
    <ActiveImage src={activeThumbnail.imgUrl} alt="colorless photograph" />
  );
};

const ActiveImage = styled.img`
  max-height: 60vh;
  display: block;
  margin: auto;
  border: 1px;
  border-color: white;
  border-style: double;
  ::selection {
    background: white; /* WebKit/Blink Browsers */
  }
  @media only screen and (max-width: 420px) {
    max-width: 98vw;
    border-color: black;
  }
`;

export default ActiveThumbnailWindow;
