import React, { Component } from "react";
import styled from "styled-components";

const StyledFooter = styled.footer`
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 5%
  color: #222;
  text-align: center;
`;

const StyledLink = styled.a`
  --mainColor: #222;

  font-size: 100%;
  background: linear-gradient(
    to bottom,
    var(--mainColor) 0%,
    var(--mainColor) 100%
  );

  background-position: 0 100%;
  background-repeat: repeat-x;
  background-size: 2px 2px;
  color: #000;
  text-decoration: none;
  transition: background-size 0.5s;
  margin-left: 10px;

  :hover {
    background-size: 4px 50px;
    color: #fff;
  }

  @media only screen and (max-width: 420px) {
    display: none;
  }
`;

export default class Footer extends Component {
  render() {
    return (
      <StyledFooter>
        <StyledLink href="mailto:aaari94@gmail.com">Contact</StyledLink>
      </StyledFooter>
    );
  }
}
